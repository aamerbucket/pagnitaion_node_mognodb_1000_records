# Pagination - Node & MongoDb

## Load data 
* Generate and Load 1000 records from Mockaroo website
* Download the CSV file
* Import it to MongoDB
    > Database name: demo, collection: users
     ` mongoimport -d demo -c users --type csv MOCK_DATA.csv --headerline `
* Example 
> User request for page number 1 with size 10. So, in the query, we will skip no records and get the first 10 records because of its page number one.

> Math would be like this.

*** Skip = size * (page number -1) ***
*** Limit = size ***

* Run node app.js
* hit ``` http://localhost:3000/api/users?pageNo=2&size=10 ``` on postman

## Get the total number of pages
``` We need to also extract the total number of pages based on the size provided by the user. So for example, if user wants 10 records a time for 1000 records in total, then we need to tell the API consumer that you can traverse complete record in 10 pages. We can do this by getting the count of records and then dividing it by the size asked by the user. ```

* Run node server.js
* hit ``` http://localhost:3000/api/users?pageNo=2&size=10 ``` on postman
___ you will get total pages ___
